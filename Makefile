#
# Install configuration files.
#
SHELL = /bin/bash
CFGDIR = $(HOME)/config
APPCFG = bcheck.yaml

.PHONY: dpc dsc

dpc: dpc-adcfg.yaml dpc-switches power_cfg.yaml $(APPCFG)
	cp -v dpc-adcfg.yaml $(CFGDIR)/adcfg.yaml
	cp -v dpc-switches $(CFGDIR)/switches
	cp -v power_cfg.yaml $(CFGDIR)
	cp -v $(APPCFG) $(CFGDIR)
	cp -av profiles $(CFGDIR)
	cp -av powercfg $(CFGDIR)

dsc: dsc-adcfg.yaml dsc-switches
	cp -v dsc-adcfg.yaml $(CFGDIR)/adcfg.yaml
	cp -v dsc-switches $(CFGDIR)/switches
